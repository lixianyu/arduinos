
void setup() {
  Serial.begin(9600);
  Serial.println("Arduino performance test begins now.");
  pinMode(2, OUTPUT);
}

extern volatile unsigned long timer0_millis;

void loop() {
  unsigned long i = 0;
  unsigned long stop_time;
  
  stop_time = millis() + 1000;
  
  while (timer0_millis < stop_time) {
//    digitalWrite(2, HIGH);
//    digitalWrite(2, LOW);
//    bitSet(PORTD, 2);
//    bitClear(PORTD, 2);
    bitSet(PIND, 2);
    i++;
  }
  
  bitClear(PORTD, 2);
  Serial.print(i);
  Serial.println(" loops in one second.");
  while(1);
}
