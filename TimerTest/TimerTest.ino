
void setup() {
  bitSet(DDRB, 4);
  TCCR1A = 0;
  TCCR1B = 1 << CS12;
  bitSet(TIMSK1, TOIE1);
}

void loop() {
  
}

ISR(TIMER1_OVF_vect) {
  bitSet(PINB, 4);
}
