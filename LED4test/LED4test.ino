
//设置阴极接口
int a = 48;
int b = 43;
int c = 40;
int d = 34;
int e = 32;
int f = 46;
int g = 39;
int p = 36;
//设置阳极接口
int d4 = 41;
int d3 = 42;
int d2 = 33;
int d1 = 44;

//设置变量
int del = 100;
int buttoncount = 0;
int loopcount = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Enter setup()");
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(p, OUTPUT);
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
  digitalWrite(p, LOW);
  
  digitalWrite(d4, HIGH);//close d4
  digitalWrite(d3, HIGH);
  digitalWrite(d2, HIGH);
  digitalWrite(d1, HIGH);
  
  //while(1) {}
  TCCR2A = 0;
  TCCR2B = 6; // CK/256
  bitSet(TIMSK2, TOIE2);
}

int gK = 0;
void loop() {
  /*
  roulette(4); //轮转效果
  delay(100);
  //zigzag(2); //Z字型效果
  delay(100);
  //circles(4); //圈状效果
  delay(100);*/
  
  int k =-999;
  while(1)
  {
     //LED_Segments(k);
     gK++;
     k++;
     delay(1000);
  }
}

void pickDigit(int x) //定义pickDigit(x),其作用是开启dx端口
{
    digitalWrite(d1, HIGH);
    digitalWrite(d2, HIGH);
    digitalWrite(d3, HIGH);
    digitalWrite(d4, HIGH);
    switch(x)
    {
    case 1:
    digitalWrite(d1, LOW);
    break;
    case 2:
    digitalWrite(d2, LOW);
    break;
    case 3:
    digitalWrite(d3, LOW);
    break;
    default:
    digitalWrite(d4, LOW);
    break;
    }
}

void clearLEDs() //清屏
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
  digitalWrite(p, LOW);
}

void roulette(int x) //设置轮转效果
{
    loopcount = 0;
    while (loopcount < x)
    {
      digitalWrite(a, HIGH);
      pickDigit(1);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(4);
      delay(del);
      digitalWrite(a, LOW);
      digitalWrite(b, HIGH);
      delay(del);
      digitalWrite(b, LOW);
      digitalWrite(c, HIGH);
      delay(del);
      digitalWrite(c, LOW);
      digitalWrite(d, HIGH);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(1);
      delay(del);
      digitalWrite(d, LOW);
      digitalWrite(e, HIGH);
      delay(del);
      digitalWrite(e, LOW);
      digitalWrite(f, HIGH);
      delay(del);
      clearLEDs();
      loopcount++;
    }
}

void zigzag(int x) //设置Z字形效果
{
    loopcount = 0;
    while(loopcount < x)
    {
      digitalWrite(a, HIGH);
      pickDigit(1);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(4);
      delay(del);
      digitalWrite(a, LOW);
      digitalWrite(b, HIGH);
      delay(del);
      digitalWrite(b, LOW);
      digitalWrite(g, HIGH);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(1);
      delay(del);
      digitalWrite(g, LOW);
      digitalWrite(e, HIGH);
      delay(del);
      digitalWrite(e, LOW);
      digitalWrite(d, HIGH);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(4);
      delay(del);
      digitalWrite(d, LOW);
      digitalWrite(c, HIGH);
      delay(del);
      digitalWrite(c, LOW);
      digitalWrite(g, HIGH);
      delay(del);
      pickDigit(3);
      delay(del);
      pickDigit(2);
      delay(del);
      pickDigit(1);
      delay(del);
      digitalWrite(g, LOW);
      digitalWrite(f, HIGH);
      delay(del);
      clearLEDs();
      loopcount++;
    }
}
 
//设置圈状效果
void circles(int x) 
{
    loopcount = 0;
    while (loopcount < x)
    {
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(g, HIGH);
      pickDigit(1);
      delay(250);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(f, LOW);
      digitalWrite(c, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(e, HIGH);
      pickDigit(2);
      delay(250);
      digitalWrite(a, HIGH);
      digitalWrite(b, HIGH);
      digitalWrite(f, HIGH);
      digitalWrite(c, LOW);
      digitalWrite(d, LOW);
      digitalWrite(e, LOW);
      pickDigit(3);
      delay(250);
      digitalWrite(a, LOW);
      digitalWrite(b, LOW);
      digitalWrite(f, LOW);
      digitalWrite(c, HIGH);
      digitalWrite(d, HIGH);
      digitalWrite(e, HIGH);
      pickDigit(4);
      delay(250);
      clearLEDs();
      loopcount++;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned char const segs[]={48, 43, 40, 34, 32, 46, 39, 36};             //阳极管脚*a*b*c*d*e*f*g*p*
unsigned char const Pins[]={44, 33, 42, 41};                      //阴极管脚
unsigned char const seven_seg_digits[11][7]=
{{1,1,1,1,1,1,0}//*0*
,{0,1,1,0,0,0,0}//*1*
,{1,1,0,1,1,0,1}//*2*
,{1,1,1,1,0,0,1}//*3*
,{0,1,1,0,0,1,1}//*4*
,{1,0,1,1,0,1,1}//*5*
,{1,0,1,1,1,1,1}//*6*
,{1,1,1,0,0,0,0}//*7*
,{1,1,1,1,1,1,1}//*8*
,{1,1,1,1,0,1,1}//*9*
,{0,0,0,0,0,0,1}//*-*
};
unsigned char const EOOR[4][7]=
{{1,0,0,1,1,1,1}//*E*
,{1,1,1,1,1,1,0}//*O*
,{1,1,1,1,1,1,0}//*O*
,{0,1,0,0,1,1,1}//*R*
};

/*********************************/

void clearLEDss()          //***清屏***
{   
    for(int i=0;i<=7;i++)
    {
       pinMode(segs[i],OUTPUT);
       digitalWrite(segs[i],LOW);
    }

   for(int i=0;i<=3;i++)
    {
      pinMode(Pins[i],OUTPUT);
      digitalWrite(Pins[i],HIGH);
    }
}

/*********************************/

void eoor()         //***当数据超出范围，出错提示***
{
  for(int n=0;n<=3;n++)
  {
    digitalWrite(Pins[n],LOW);
    for(int l=0;l<=6;l++)
        {digitalWrite(segs[l],EOOR[n][l]);}
    delay(4);
    digitalWrite(Pins[n],HIGH); 
  }
}

/*********************************/

void LED_Word(int n,int m)//***显示 第n位 数字m ***
{    
     for(int r=0;r<=100;r++)
     {
      digitalWrite(Pins[n],LOW);
      for(int x=0;x<=6;x++)
         {digitalWrite(segs[x],seven_seg_digits[m][x]);}
     }
     digitalWrite(Pins[n],HIGH);
}

/******************************/

void LED_Segments(short int a)//***显示-999~9999之间的数***
{
  //clearLEDss();
  if((a <= 9999)&&(a >= -999))    //***
   {   
       int q, w, e, r;    
       q = abs(a/1000);           //***千位（负数的是负号）***
       w = abs((a/100)%10);       //***百位***
       e = abs((a/10)%10);        //***十位***
       r = abs(a%10);             //***个位***
          {if(a < 0) {q = 10;}}
       char u[]={q,w,e,r};
       for(int p=0;p<=100;p++)    //？？做了个循环，避免闪烁，但是效果一般，不知道有没有其他办法？求教
         {
         for(int y=0;y<=3;y++)
           {LED_Word(y,u[y]);}
         }
     }
  else
    {eoor();}                       //***出错提示***
    delay(1);
    //while(1) {}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//Arduino Internals : page 176
volatile byte seconds_units = 0;
volatile byte seconds_tens = 0;
volatile byte minutes_units = 0;
volatile byte minutes_tens = 0;
volatile byte hours_units = 2;
volatile byte hours_tens = 1;

const byte numerals[10] = {
  0b00111111, // 0
  0b00000110, // 1
  0b01011011, // 2
  0b01001111, // 3
  0b01100110, // 4
  0b01101101, // 5
  0b01111101, // 6
  0b00000111, // 7
  0b01111111, // 8
  0b01101111, // 9
};

void drawSegment(byte index) {
  digitalWrite(a, numerals[index] & 0x01); // a
  digitalWrite(b, numerals[index] & 0x02); // b
  digitalWrite(c, numerals[index] & 0x04); // c
  digitalWrite(d, numerals[index] & 0x08); // d
  digitalWrite(e, numerals[index] & 0x10); // e
  digitalWrite(f, numerals[index] & 0x20); // f
  digitalWrite(g, numerals[index] & 0x40); // g
}

ISR(TIMER2_OVF_vect) {
  static byte digit = 0;
  int q, w, e, r;
  q = abs(gK / 1000);           //***千位（负数的是负号）***
  w = abs((gK / 100) % 10);     //***百位***
  e = abs((gK / 10) % 10);      //***十位***
  r = abs(gK % 10);             //***个位***
  if (gK < 0) {
    q = 10;
  }
  switch (digit) {
    case 0 :
    digitalWrite(p, HIGH);
    digitalWrite(d1, HIGH);
    drawSegment(r);
    digitalWrite(d4, LOW);
    break;
    
    case 1:
    digitalWrite(p, LOW);
    digitalWrite(d4, HIGH);
    drawSegment(e);
    digitalWrite(d3, LOW);
    break;
    
    case 2:
    digitalWrite(p, LOW);
    digitalWrite(d3, HIGH);
    drawSegment(w);
    digitalWrite(d2, LOW);
    break;
    
    case 3:
    digitalWrite(p, HIGH);
    digitalWrite(d2, HIGH);
    drawSegment(q);
    digitalWrite(d1, LOW);
    break;
  }
  digit++;
  if (digit > 3) {
    digit = 0;
  }
}
