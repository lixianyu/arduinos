
void setup() {
  Serial.begin(9600);
  Serial.println("Arduino Uno as Thermometer");
  ADMUX = 1<<REFS1 | 1<<REFS0 | 1<<MUX3;
  ADCSRA = 1<<ADEN | 1<<ADSC |0x07;
}

#define OFFSET 343
void loop() {
  Serial.print(ADC - OFFSET);
  Serial.println("C");
  bitSet(ADCSRA, ADSC);
  delay(1000);
}
