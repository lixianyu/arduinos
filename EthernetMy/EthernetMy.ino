#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>
#include <math.h>


// for yeelink api
#define APIKEY         "194c80e279251cdd08461c7668a7194f" // replace your yeelink api key here
#define DEVICEID       3264 // replace your device ID
#define SENSORID       4581 // replace your sensor ID

// assign a MAC address for the ethernet controller.
byte mac[] = { 0x00, 0x1D, 0x72, 0x82, 0x35, 0x9E};
// initialize the library instance:
EthernetClient client;
char server[] = "api.yeelink.net";   // name address for yeelink API

void setup() {
  Serial.begin(57600);
  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(6, OUTPUT);
  // start the Ethernet connection with DHCP:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    for(;;)
      ;
  }
  else {
    Serial.println("Ethernet configuration OK");
  }
}

int static gCount = 0;
void loop() {
  Serial.print("Enter loop(), gCount = ");
  Serial.println(gCount);
//  int curTime = millis();
  
  if (gCount % 15 == 0) {
    float sensorReading;
    // sensorReading = readLightSensor();
    sensorReading = readLM35DZ();
    //send data to server
    sendData(sensorReading);
  }
  if (gCount % 3 == 0) {
    digitalWrite(8, HIGH);
    delay(100);
    digitalWrite(8, LOW);
  }
  delay(950);
  gCount++;
}

// this method makes a HTTP connection to the server:
void sendData(float thisData) {
  Serial.println("Enter  sendData()");
  // if there's a successful connection:
  if (client.connect(server, 80)) {
    digitalWrite(6, LOW);
    Serial.println("connecting...");
    // send the HTTP PUT request:
    client.print("POST /v1.0/device/");
    client.print(DEVICEID);
    client.print("/sensor/");
    client.print(SENSORID);
    client.print("/datapoints");
    client.println(" HTTP/1.1");
    client.println("Host: api.yeelink.net");
    client.print("Accept: *");
    client.print("/");
    client.println("*");
    client.print("U-ApiKey: ");
    client.println(APIKEY);
    client.print("Content-Length: ");

    // calculate the length of the sensor reading in bytes:
    // 8 bytes for {"value":} + number of digits of the data:
    //int thisLength = 10 + getLength(thisData);
    int thisLength = 10 + 5;
    client.println(thisLength);

    client.println("Content-Type: application/x-www-form-urlencoded");
    client.println("Connection: close");
    client.println();

    // here's the actual content of the PUT request:
    client.print("{\"value\":");
    client.print(thisData);
    client.println("}");
  }
  else {
    digitalWrite(6, HIGH);
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
  }
  delay(150);
  client.stop();
  // note the time that the connection was made or attempted:
  //lastConnectionTime = millis();
  Serial.println("Leave  sendData()");
}

float readLM35DZ() {
  int iLED = 7;
  int i=analogRead(1);//读取温度传感器电压值
  Serial.print("analogRead 1 is : ");
  Serial.println(i);
  float ftemp = i / 204.8;
  float tempnow = (ftemp * 1000 + 5) / 10;
  Serial.print("The current temperature is ");
  Serial.print(tempnow);
  Serial.println(" C");
  
  digitalWrite(iLED, HIGH);
  delay(200);
  digitalWrite(iLED, LOW);
  return tempnow;
}
