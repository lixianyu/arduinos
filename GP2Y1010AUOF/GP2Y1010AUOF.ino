const int dustPin = 4;
int dustVal = 0;

const int ledPower = 7;
const int delayTime = 200;
const int delayTime2 = 40;
const float offTime = 9680;

const int led = 9;

int ledState = LOW; // ledState used to set the LED
long previousMillis = 0; // will store last time LED was updated
long interval = 1000;

long previousMillisGP2Y1010AUOF = 0;
long intervalGP2Y1010AUOF = 5000;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(ledPower, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH);
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillisGP2Y1010AUOF > intervalGP2Y1010AUOF) {
    previousMillisGP2Y1010AUOF = currentMillis;
    digitalWrite(ledPower, LOW); // power on the LED
    delayMicroseconds(delayTime);
    dustVal = analogRead(dustPin); // read the dust value via pin 4 on the sensor
    //delayMicroseconds(delayTime2);
    digitalWrite(ledPower, HIGH); // turn the LED off
    delayMicroseconds(offTime);
    
    digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    delay(100);
    digitalWrite(led, HIGH);
    Serial.println(dustVal);
    delay(100);
    digitalWrite(led, LOW);
    delay(780);
    
    
  } else {
    digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(100);
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    delay(1000);             // wait for a second
  }
  
  /*
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }
    digitalWrite(led, ledState);
  }*/
}
