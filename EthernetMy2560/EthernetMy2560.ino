#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>
#include <math.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <dht11.h>

// For 18B20 : https://github.com/milesburton/Arduino-Temperature-Control-Library
// for yeelink api
#define APIKEY         "194c80e279251cdd08461c7668a7194f" // replace your yeelink api key here
#define DEVICEID       3379 // replace your device ID
#define SENSORID       4717 // replace your sensor ID

// assign a MAC address for the ethernet controller.
byte mac[] = { 0x00, 0x1D, 0x72, 0x82, 0x35, 0x33 };
// initialize the library instance:
EthernetClient client;
char server[] = "api.yeelink.net";   // name address for yeelink API
//DallasTemperature tempSensor; // You may instantiate as many copies as you require.(18B20)
// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 47

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature tempSensor(&oneWire);

dht11 DHT11;
#define DHT11PIN 45

//////////////////////////////////
	//设置阴极接口
	int a = 48;
	int b = 43;
	int c = 40;
	int d = 34;
	int e = 32;
	int f = 46;
	int g = 39;
        int p = 36;
	//设置阳极接口
	int d4 = 41;
	int d3 = 42;
	int d2 = 33;
	int d1 = 44;

void setup() {
  Wire.begin();
  Serial.begin(9600);
  tempSensor.begin();
  tempSensor.setResolution(12);

  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);
  pinMode(53, OUTPUT);
  digitalWrite(53, HIGH);

  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  digitalWrite(7, HIGH);
  digitalWrite(8, HIGH);

  pinMode(6, OUTPUT);
  Serial.println("Enter setup()");
  //  tempSensor = new DallasTemperature();
  //pinMode(47, OUTPUT);

  // start the Ethernet connection with DHCP:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    for(;;)
      ;
  }
  else {
    Serial.println("Ethernet configuration OK");
  }

  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(p, OUTPUT);
/*
  TCCR2A = 0;
  TCCR2B = 6; // CK/256
  bitSet(TIMSK2, TOIE2);
  */
}

int static gCount = 0;
int static gWhichSensor = 0;
int static gSensorID = 0;
void loop() {
  Serial.print("Enter loop(), gCount = ");
  Serial.println(gCount);
  //  int curTime = millis();

  if (gCount % 10 == 0) {
    float sensorReading;
    // sensorReading = readLightSensor();
    if (gWhichSensor == 0) {
      sensorReading = readLM35DZ();
      gWhichSensor++;
      gSensorID = 4717;
    }
    else if (gWhichSensor == 1) {
      sensorReading = readDS18B20();
      gWhichSensor++;
      if (gWhichSensor == -256) goto NotSendData;
      gSensorID = 4716;
    }
    else if (gWhichSensor == 2) {
      sensorReading = readDHT11(true);//Read temperature
      gWhichSensor++;
      gSensorID = 4715;
    }
    else if (gWhichSensor == 3) {
      sensorReading = readDHT11(false);//Read humidity
      gWhichSensor = 0;
      if (sensorReading <= 0) goto NotSendData;
      gSensorID = 4718;
    }
    //send data to server
    sendData(sensorReading, gSensorID);
NotSendData:
    digitalWrite(7, HIGH);
    delay(200);
    digitalWrite(7, LOW);
  }
  if (gCount % 3 == 0) {
    digitalWrite(8, HIGH);
    delay(100);
    digitalWrite(8, LOW);
  }
  
  delay(900);
  gCount++;
  showLED4();
}

int readDHT11(boolean TorH) {
  int ret = 0;
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT11LIB_VERSION);
  int chk = DHT11.read(DHT11PIN);

  Serial.print("Read sensor: ");
  switch (chk)
  {
    case DHTLIB_OK: 
		Serial.println("OK");
		break;
    case DHTLIB_ERROR_CHECKSUM: 
		Serial.println("Checksum error");
                return 0;
		break;
    case DHTLIB_ERROR_TIMEOUT: 
		Serial.println("Time out error");
                return 0;
		break;
    default: 
		Serial.println("Unknown error"); 
                return 0;
		break;
  }

  if (TorH == true) {
    ret = DHT11.temperature;
  }
  else {
    ret = DHT11.humidity;
  }
  Serial.print("Humidity (%): ");
  Serial.println((float)DHT11.humidity, 2);

  Serial.print("Temperature (oC): ");
  Serial.println((float)DHT11.temperature, 2);
/*
  Serial.print("Temperature (oF): ");
  Serial.println(Fahrenheit(DHT11.temperature), 2);

  Serial.print("Temperature (K): ");
  Serial.println(Kelvin(DHT11.temperature), 2);

  Serial.print("Dew Point (oC): ");
  Serial.println(dewPoint(DHT11.temperature, DHT11.humidity));

  Serial.print("Dew PointFast (oC): ");
  Serial.println(dewPointFast(DHT11.temperature, DHT11.humidity));
*/
  return ret;
}

float readLM35DZ() {
  int i=analogRead(1);//读取温度传感器电压值
  Serial.print("analogRead 1 is : ");
  Serial.println(i);
  float ftemp = i / 204.8;
  float tempnow = (ftemp * 1000 + 5) / 10;
  Serial.print("The current temperature is ");
  Serial.print(tempnow);
  Serial.println(" C");
  return tempnow;
}

float readDS18B20() {
  static float lastValue = -256;
  // Ask the library whether the device is valid
  /*
  switch(tempSensor.isValid())
   {
   case 1:
   Serial.println("Invalid CRC");
   tempSensor.reset(); // Attempts to redetect IC
   return -100;
   case 2:
   Serial.println("Not a valid device");
   tempSensor.reset(); // Attempts to redetect IC
   return -100;
   }
   */
  Serial.println("Enter readDS18B20()");
  tempSensor.requestTemperatures(); // Send the command to get temperatures
  Serial.print(tempSensor.getTempCByIndex(0));
  Serial.print("C");
  Serial.println();
  Serial.print(DallasTemperature::toFahrenheit(tempSensor.getTempCByIndex(0))); // Use the inbuild Celcius to Fahrenheit conversion. Returns a float
  Serial.print("F");
  Serial.println();
  
  float ret;
  if (lastValue == -256) {
    Serial.println("OH,my god, lastValue equal -256");
    ret = tempSensor.getTempCByIndex(0);
    lastValue = ret;
    Serial.println("Leave readDS18B20()...000");
    return ret;
  }
  else {
    ret = tempSensor.getTempCByIndex(0);
    if ((abs(lastValue - ret)) < 10) {
      lastValue = ret;
      Serial.println("Leave readDS18B20()...001");
      return ret;
    }
  }
  Serial.println("Leave readDS18B20()...002");
  return -256;
}

// this method makes a HTTP connection to the server:
void sendData(float thisData, int sensorid) {
  Serial.println("Enter  sendData()");
  // if there's a successful connection:
  if (client.connect(server, 80)) {
    digitalWrite(6, LOW);
    Serial.println("connecting...");
    // send the HTTP PUT request:
    client.print("POST /v1.0/device/");
    client.print(DEVICEID);
    client.print("/sensor/");
    client.print(sensorid);
    client.print("/datapoints");
    client.println(" HTTP/1.1");
    client.println("Host: api.yeelink.net");
    client.print("Accept: *");
    client.print("/");
    client.println("*");
    client.print("U-ApiKey: ");
    client.println(APIKEY);
    client.print("Content-Length: ");

    // calculate the length of the sensor reading in bytes:
    // 8 bytes for {"value":} + number of digits of the data:
    //int thisLength = 10 + getLength(thisData);
    int thisLength = 10 + 5;
    client.println(thisLength);

    client.println("Content-Type: application/x-www-form-urlencoded");
    client.println("Connection: close");
    client.println();

    // here's the actual content of the PUT request:
    client.print("{\"value\":");
    client.print(thisData);
    client.println("}");
  }
  else {
    digitalWrite(6, HIGH);
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
  }
  delay(150);
  client.stop();
  // note the time that the connection was made or attempted:
  //lastConnectionTime = millis();
  Serial.println("Leave  sendData()");
}


//////////////////////////////设置变量
	long n = 0;
	int x = 1;
	int del = 55;  //此处数值对时钟进行微调
void showLED4() {
          digitalWrite(d1, HIGH);//close 
	  digitalWrite(d2, HIGH);
	  digitalWrite(d3, HIGH);
	  digitalWrite(d4, HIGH);
          //Serial.print("n = ");
          //Serial.println(n);
	  for (int i = 0; i < 400; i++) {
clearLEDs();
	  pickDigit(1);
	  pickNumber((n/x/1000)%10);
	  //delayMicroseconds(del);
	 
clearLEDs();
	  pickDigit(2);
	  pickNumber((n/x/100)%10);
	  //delayMicroseconds(del);
	 
clearLEDs();
	  pickDigit(3);
	  dispDec(3);
	  pickNumber((n/x/10)%10);
	  //delayMicroseconds(del);
	 
clearLEDs();
	  pickDigit(4);
          pickNumber(n/x%10);
	  //delayMicroseconds(del);
	 }
	  n++;
	 
	  /*if (digitalRead(13) == LOW)
	  {
	    n = 0;
	  }*/
}

	void pickDigit(int x)  //定义pickDigit(x),其作用是开启dx端口
	{
	  digitalWrite(d1, HIGH);
	  digitalWrite(d2, HIGH);
	  digitalWrite(d3, HIGH);
	  digitalWrite(d4, HIGH);
	 
	  switch(x)
	  {
	  case 1: 
	    digitalWrite(d1, LOW);
            /*digitalWrite(d2, HIGH);
	    digitalWrite(d3, HIGH);
	    digitalWrite(d4, HIGH);*/
	    break;
	  case 2: 
	    digitalWrite(d2, LOW);
/*            digitalWrite(d1, HIGH);
	    digitalWrite(d3, HIGH);
	    digitalWrite(d4, HIGH);*/
	    break;
	  case 3: 
	    digitalWrite(d3, LOW); 
/*            digitalWrite(d2, HIGH);
	    digitalWrite(d1, HIGH);
	    digitalWrite(d4, HIGH);*/
	    break;
	  default: 
	    digitalWrite(d4, LOW); 
/*            digitalWrite(d2, HIGH);
	    digitalWrite(d3, HIGH);
	    digitalWrite(d1, HIGH);*/
	    break;
	  }
	}
	 
	void pickNumber(int x)   //定义pickNumber(x),其作用是显示数字x
	{
	  switch(x)
	  {
	  default: 
	    zero(); 
	    break;
	  case 1: 
	    one(); 
	    break;
	  case 2: 
	    two(); 
	    break;
	  case 3: 
	    three(); 
	    break;
	  case 4: 
	    four(); 
	    break;
	  case 5: 
	    five(); 
	    break;
	  case 6: 
	    six(); 
	    break;
	  case 7: 
	    seven(); 
	    break;
	  case 8: 
	    eight(); 
	    break;
	  case 9: 
	    nine(); 
	    break;
	  }
	}
	 
	void dispDec(int x)  //设定开启小数点
	{
	  digitalWrite(p, HIGH);
	}
	 
	void clearLEDs()  //清屏
	{
	  digitalWrite(a, LOW);
	  digitalWrite(b, LOW);
	  digitalWrite(c, LOW);
	  digitalWrite(d, LOW);
	  digitalWrite(e, LOW);
	  digitalWrite(f, LOW);
	  digitalWrite(g, LOW);
	  digitalWrite(p, LOW);
	}
	 
	void zero()  //定义数字0时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, HIGH);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, LOW);
	}
	 
	void one()  //定义数字1时阴极那些管脚开关
	{
	  digitalWrite(a, LOW);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, LOW);
	  digitalWrite(e, LOW);
	  digitalWrite(f, LOW);
	  digitalWrite(g, LOW);
	}
	 
	void two()  //定义数字2时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, LOW);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, HIGH);
	  digitalWrite(f, LOW);
	  digitalWrite(g, HIGH);
	}
	 
	void three()  //定义数字3时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, LOW);
	  digitalWrite(f, LOW);
	  digitalWrite(g, HIGH);
	}
	 
	void four()  //定义数字4时阴极那些管脚开关
	{
	  digitalWrite(a, LOW);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, LOW);
	  digitalWrite(e, LOW);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, HIGH);
	}
	 
	void five()  //定义数字5时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, LOW);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, LOW);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, HIGH);
	}
	 
	void six()  //定义数字6时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, LOW);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, HIGH);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, HIGH);
	}
	 
	void seven()  //定义数字7时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, LOW);
	  digitalWrite(e, LOW);
	  digitalWrite(f, LOW);
	  digitalWrite(g, LOW);
	}
	 
	void eight()  //定义数字8时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, HIGH);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, HIGH);
	}
	 
	void nine()  //定义数字9时阴极那些管脚开关
	{
	  digitalWrite(a, HIGH);
	  digitalWrite(b, HIGH);
	  digitalWrite(c, HIGH);
	  digitalWrite(d, HIGH);
	  digitalWrite(e, LOW);
	  digitalWrite(f, HIGH);
	  digitalWrite(g, HIGH);
	}
//--------------------------------------------------------------------------------------------------------
const byte numerals[10] = {
  0b00111111, // 0
  0b00000110, // 1
  0b01011011, // 2
  0b01001111, // 3
  0b01100110, // 4
  0b01101101, // 5
  0b01111101, // 6
  0b00000111, // 7
  0b01111111, // 8
  0b01101111, // 9
};

void drawSegment(byte index) {
  digitalWrite(a, numerals[index] & 0x01); // a
  digitalWrite(b, numerals[index] & 0x02); // b
  digitalWrite(c, numerals[index] & 0x04); // c
  digitalWrite(d, numerals[index] & 0x08); // d
  digitalWrite(e, numerals[index] & 0x10); // e
  digitalWrite(f, numerals[index] & 0x20); // f
  digitalWrite(g, numerals[index] & 0x40); // g
}

ISR(TIMER2_OVF_vect) {
  static byte digit = 0;
  int q, w, e, r;
  q = abs(gCount / 1000);           //***千位（负数的是负号）***
  w = abs((gCount / 100) % 10);     //***百位***
  e = abs((gCount / 10) % 10);      //***十位***
  r = abs(gCount % 10);             //***个位***
  if (gCount < 0) {
    q = 10;
  }
  switch (digit) {
    case 0 :
    digitalWrite(p, HIGH);
    digitalWrite(d1, HIGH);
    drawSegment(r);
    digitalWrite(d4, LOW);
    break;
    
    case 1:
    digitalWrite(p, LOW);
    digitalWrite(d4, HIGH);
    drawSegment(e);
    digitalWrite(d3, LOW);
    break;
    
    case 2:
    digitalWrite(p, LOW);
    digitalWrite(d3, HIGH);
    drawSegment(w);
    digitalWrite(d2, LOW);
    break;
    
    case 3:
    digitalWrite(p, HIGH);
    digitalWrite(d2, HIGH);
    drawSegment(q);
    digitalWrite(d1, LOW);
    break;
  }
  digit++;
  if (digit > 3) {
    digit = 0;
  }
}
