void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  digitalWrite(14, HIGH);
}

void loop() {
  int va = analogRead(0);
  Serial.println(va);
  if (va < 900) {
    digitalWrite(13, HIGH);
    delay(100);
  }
  else {
    digitalWrite(13, LOW);
  }
  delay(1000);
}
