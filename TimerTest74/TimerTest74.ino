
void setup() {
  bitSet(DDRB, 4);
  TCCR1A = 0;
  TCCR1B = 1 << WGM13 | 1<<WGM12 | 1<<CS12 | 1<<CS10;
  ICR1 = 15625;
  bitSet(TIMSK1, ICIE1);
}

void loop() {
}

ISR(TIMER1_CAPT_vect) {
  bitSet(PINB, 4);
  delay(100);
  bitClear(PINB, 4);
}
