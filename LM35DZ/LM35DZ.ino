/*
 To test LM35DZ sensor.
 */
 
 const int led = 9;
 
 
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  //Serial.begin(9600);
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  /*
  // read the input on analog pin 0:
  int n = analogRead(A0);
  // print out the value you read:
  Serial.println(n);
  
  float vol = n * (5.0 / 1024.0 * 100.0); // Please go to http://0.gg/yI8nk to see theory
  Serial.print("The temperature is : ");
  Serial.print(vol);
  Serial.println(" C.");
  */
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(20);
  //delayMicroseconds(1000);
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(2000);             // wait for a second
}
