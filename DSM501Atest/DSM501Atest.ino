//DSM501A DUST
#define DSMPinBigerThan1_0  6 //大于1.0微米的颗粒数
#define DSMPinBigerThan2_5  7 //大于2.5微米的颗粒数
#define LEDpin 8
unsigned long previousMillis = 0;        //前一次判断时间点
unsigned long partMillis=0;              //到溢出时计算的时间
const unsigned long sectev = 30;           // 时间间隔(秒)
const unsigned long interval = 30000;           // 时间间隔(毫秒) = sectev * 1000
const unsigned long mintev = 30000000; // 时间间隔(微秒) =interval * 1000

void setup() {
  Serial.begin(9600);
  pinMode(DSMPinBigerThan1_0, INPUT); 
  pinMode(DSMPinBigerThan2_5, INPUT);
  pinMode(LEDpin, OUTPUT);
  digitalWrite(LEDpin, HIGH);
  Serial.println("Begin DSM501A warmming");
  delay(60000);
}

void loop() {
  Serial.println("Enter loop()");
  boolean goloop=true;
  unsigned long currentMillis;
  long tempBigerThan2_5=0;
  long tempBigerThan1_0=0;
  unsigned long durationBigerThan2_5=0;
  unsigned long durationBigerThan1_0=0;

  while(goloop)      //loop
  {
    currentMillis = micros();
    if(currentMillis<previousMillis)
    {
      partMillis = 4294967295 -  previousMillis +1;
      previousMillis = 0;  
    }

    if(currentMillis - previousMillis - partMillis < mintev)
    {
      //大于2.5微米的颗粒数
      if(tempBigerThan2_5==0)
      {
        tempBigerThan2_5=-1;
        tempBigerThan2_5=pulseIn(DSMPinBigerThan2_5, LOW);
      }

      if(tempBigerThan2_5>0)
      {
        durationBigerThan2_5 =durationBigerThan2_5 + tempBigerThan2_5;
        tempBigerThan2_5=0;
      }

      //大于1.0微米的颗粒数
      if(tempBigerThan1_0==0)
      {
        tempBigerThan1_0=-1;
        tempBigerThan1_0=pulseIn(DSMPinBigerThan1_0, LOW);
      }

      if(tempBigerThan1_0>0)
      {
        durationBigerThan1_0 =durationBigerThan1_0 + tempBigerThan1_0;
        tempBigerThan1_0=0;
      }
    }
    else
    {
      goloop=false;
      previousMillis = currentMillis;
    }
  }
  double per =double(durationBigerThan2_5)/double(interval);// had multiply 1000
  int pcsBigerThan2_5 = -1;
  pcsBigerThan2_5 =  per * 50.0;
  per =double(durationBigerThan1_0)/double(interval);// had multiply 1000
  int pcsBigerThan1_0 = -1;
  pcsBigerThan1_0 =  per * 50.0;
  Serial.print("pcs biger than 2.5 = ");
  Serial.print(pcsBigerThan2_5);
  Serial.print("\t");
  Serial.print("pcs biger than 1.0 = ");
  Serial.println(pcsBigerThan1_0);
  
  digitalWrite(LEDpin, HIGH);
  delay(4000);
  digitalWrite(LEDpin, LOW);
}

