/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 2;

// the setup routine runs once when you press reset:
void setup() {
  Serial.begin(9600);
  Serial.print("F_CPU = ");
  Serial.println(F_CPU);  
  // initialize the digital pin as an output.
//  pinMode(led, OUTPUT);
  bitSet(DDRD, 2);
//  DDRD = 1<<2;
}

extern volatile unsigned long timer0_millis;
// the loop routine runs over and over again forever:
void loop() {
//  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
//  delay(1000);               // wait for a second
//  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  bitSet(PIND, 2);
//  delay(1000);               // wait for a second
//  for (volatile int i = 0; i < 32600; i++);
  while (timer0_millis < 1000);
  timer0_millis = 0;
}
