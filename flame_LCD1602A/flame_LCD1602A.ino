// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

const int pinBuzzer = 8;
const int pinHuoyan = 5;
const int pinLED = 7;
volatile unsigned long g_seconds = 0;
volatile unsigned long g_minutes = 0;
volatile unsigned long g_hours = 0;
volatile unsigned long g_leftSeconds = 0;
volatile unsigned long gBeginTimeSeconds = 0;
void setup()
{
  gBeginTimeSeconds = getSecond(11,0);
  //Serial.begin(9600);
  //Serial.println("Enter setup()");
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("init");
  //
  pinMode(pinBuzzer, OUTPUT);//设置数字pinBuzzer引脚为输出方式
  pinMode(pinLED, OUTPUT);
  digitalWrite(pinLED, LOW);
  
  //Serial.println("Leave setup()");
}

void initTimer() {
  TCCR1A = 0;
  TCCR1B = 1 << WGM13 | 1<<WGM12 | 1<<CS12 | 1<<CS10;
  ICR1 = 15625;
  bitSet(TIMSK1, ICIE1);
}

long getSecond(long hour, long minute) {
  return ((hour * 60 * 60) + (minute * 60));
}

ISR(TIMER1_CAPT_vect) {
  //Serial.println(gBeginTimeSeconds);
  /*gBeginTimeSeconds++;
  if ( gBeginTimeSeconds > 86400) {
    gBeginTimeSeconds = 0;
  }*/
  loop2(gBeginTimeSeconds);
}    

volatile unsigned long g_BeginTime = 0;
volatile boolean g_flag = true;
volatile int gi = 0;
void loop()
{
    int frames[5];
    //int i = analogRead(pinHuoyan);//读取没有火焰时模拟口的电压值
    //delay(1000);
    int jj = 0;
    while (jj < 5) {
      frames[jj] = analogRead(pinHuoyan);
      delay(1000);
      jj++;
      if (jj == 1) {
        lcd.setCursor(0, 0);
        lcd.print("init.");
      }
      if (jj == 2) {
        lcd.setCursor(0, 0);
        lcd.print("init..");
      }
      if (jj == 3) {
        lcd.setCursor(0, 0);
        lcd.print("init...");
      }
      if (jj == 4) {
        lcd.setCursor(0, 0);
        lcd.print("init....");
      }
      if (jj == 5) {
        lcd.setCursor(0, 0);
        lcd.print("init.....");
      }
    }
    int sum = 0;
    for (int i = 0; i < 5; i++) {
      sum += frames[i];
    }
    gi = sum / 5;
    
    //Serial.print("init voltage is : ");
    //Serial.println(i);
    g_BeginTime = millis() / 1000;
    showtimenow();
    initTimer();
    while (1)
    {
      //loop1(gi);
    } 
}

/*
void loop1(int i) {
  int k;
  int j = analogRead(pinHuoyan);//不断的读取模拟口的电压值，时时监测
  //Serial.print("Current is : ");
  //Serial.println(j);
  k = j - i;//做差值
  //Serial.print("k is : ");
  //Serial.println(k);
  showFlame(i, j);
  //if (k > 123) //如果差值大于0.6（0.6为模拟值，123为对应的数字二进制值）说明有火焰
  if (k > 123)
  {
      //buzzer();//蜂鸣器发出声音
      ledBlink();
      if (g_flag == true) {
        g_BeginTime = millis() / 1000;
        g_flag = false;
      }
      showtimenow();
  }
  else
  {
      //digitalWrite(pinBuzzer, LOW);//设置数字8口为低电平，蜂鸣器不响
      digitalWrite(pinLED, HIGH);
  }
  delay(800);
}
*/

void loop2(long theSeconds) {
  int i = gi;
  int k;
  int j = analogRead(pinHuoyan);//不断的读取模拟口的电压值，时时监测
  //Serial.print("Current is : ");
  //Serial.println(j);
  k = j - i;//做差值
  //Serial.print("k is : ");
  //Serial.println(k);
  //if (k > 123) //如果差值大于0.6（0.6为模拟值，123为对应的数字二进制值）说明有火焰
  if (k > 99)
  {
      //buzzer();//蜂鸣器发出声音
      if (g_flag == true) {
        g_BeginTime = millis() / 1000;
        g_flag = false;
      }
      ledBlink();
      showtimenow();
  }
  else
  {
      //digitalWrite(pinBuzzer, LOW);//设置数字8口为低电平，蜂鸣器不响
      digitalWrite(pinLED, HIGH);
  }
  showFlame(i, j);
  //showRealTimeNow(theSeconds);
}

void showtimenow() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  //lcd.print(millis()/1000);
  //lcd.print(":");
  /////////////////////////////
  g_seconds = (millis() / 1000) - g_BeginTime;
  g_hours = g_seconds /(3600);
  g_leftSeconds = g_seconds % (3600);
  g_minutes = g_leftSeconds / 60;
  g_seconds = g_leftSeconds % 60;
  
  //Begin show on 1602 LCD
  if (g_hours < 10) {
    lcd.print("0");
  }
  lcd.print(g_hours);
  lcd.print(":");
  if (g_minutes < 10) {
    lcd.print("0");
  }
  lcd.print(g_minutes);
  lcd.print(":");
  if (g_seconds < 10) {
    lcd.print("0");
  }
  lcd.print(g_seconds);
}

void showRealTimeNow(long theSeconds) {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(8, 1);
  // print the number of seconds since reset:
  //lcd.print(millis()/1000);
  //lcd.print(":");
  /////////////////////////////
  /*
  g_seconds = gBeginTimeSeconds++;
  if (g_seconds > 59) {
    g_seconds = 0;
    g_minutes++;
    if (g_minutes > 59) {
      g_minutes = 0;
      g_hours++;
      if (g_hours > 23) {
        g_hours = 0;
      }
    }
  }
  */
  //Serial.print("Enter showRealTimeNow() : ");
  //Serial.println(theSeconds);
  //int g_seconds = gBeginTimeSeconds;
  int hours = theSeconds /(3600);
  int leftSeconds = theSeconds % (3600);
  int minutes = leftSeconds / 60;
  int seconds = leftSeconds % 60;
  //Begin show on 1602 LCD
  if (hours < 10) {
    lcd.print("0");
  }
  lcd.print(hours);
  lcd.print(":");
  if (minutes < 10) {
    lcd.print("0");
  }
  lcd.print(minutes);
  lcd.print(":");
  if (seconds < 10) {
    lcd.print("0");
  }
  lcd.print(seconds);
}

void showFlame(int initFlame, int flame) {
//  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("init:");
  lcd.print(initFlame);
  lcd.print(" fm:");
  lcd.print(flame);
  if (flame < 10) {
    lcd.print("   ");
  }
  else if (flame < 100) {
    lcd.print("  ");
  }
  else if (flame < 1000) {
    lcd.print(" ");
  }
}

void buzzer()//蜂鸣器发出“嘀”声音子程序
{
  int g;
  for(g=0;g<80;g++)    
  {
    digitalWrite(pinBuzzer, HIGH);//发声音
    delay(1);//延时1ms
    digitalWrite(pinBuzzer, LOW);//不发声音
    delay(1);//延时ms
  }
}

void ledBlink() {
  digitalWrite(pinLED, HIGH);
  delay(150);
  digitalWrite(pinLED, LOW);
}
