#define ECHOPIN 12
#define TRIGPIN 11
#define LEDpin 2
void setup()
{
  Serial.begin(9600);
  pinMode(ECHOPIN, INPUT);
  pinMode(TRIGPIN, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(LEDpin, OUTPUT);
  digitalWrite(LEDpin, LOW);
} 

void loop()
{
  digitalWrite(TRIGPIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW);
  
  float distance = pulseIn(ECHOPIN, HIGH);
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  distance= distance/58;
  Serial.println(distance);
  delay(200);
  if(distance>100) {
    digitalWrite(13,HIGH);
  }
  else {
    digitalWrite(13,LOW);
  }
  digitalWrite(LEDpin, HIGH);
  delay(110);
  digitalWrite(LEDpin, LOW);
  delay(890);
}
