int led = 7;

void setup() {                
  Serial.begin(9600);
  Serial.println("Enter setup()");
  pinMode(led, OUTPUT);
  bitSet(PCICR, PCIE2);
  bitSet(PCMSK2, PCINT22);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for a second
}

ISR(PCINT2_vect) {
  if (digitalRead(led) == HIGH) {
    Serial.println("LED is on.");
  }
  else {
    Serial.println("LED is off.");
  }
}
