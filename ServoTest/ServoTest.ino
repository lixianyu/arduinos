#include <Servo.h>

Servo myServo;  // create a servo object

//int potVal;  // variable to read the value from the analog pin
static int angle=0;   // variable to hold the angle for the servo motor

void setup() {
  Serial.begin(9600); // open a serial connection to your computer
  myServo.attach(9); // attaches the servo on pin 9 to the servo object
  bitSet(DDRD, 2);
}

static boolean gbControl = true;
void loop() {
  // scale the numbers from the pot
  //angle = 0; //0~179

  // print out the angle for the servo motor
  Serial.print("angle: ");
  Serial.println(angle);

  // set the servo position
  if (gbControl) {
    myServo.write(angle);
    angle += 5;
//    myServo.write(180);
//    gbControl = false;
  }
  else {
    myServo.write(angle--);
    angle -= 5;
//    myServo.write(0);
//    gbControl = true;
  }

  if (angle > 179) {
    gbControl = false;
    angle = 180;
    myServo.write(0);
  }
  else if (angle < 0) {
    gbControl = true;
    angle = 0;
    myServo.write(180);
  }

  // wait for the servo to get there

//  bitSet(PIND, 2);
  digitalWrite(2, HIGH);
  delay(100);
  digitalWrite(2, LOW);
  delay(900);
}




